package com.example.greeting.worker

import greeting.activity.HelloActivityImpl
import greeting.share.Queue
import greeting.workflow.GreetingWorkflowImpl
import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.serviceclient.WorkflowServiceStubsOptions
import io.temporal.worker.Worker
import io.temporal.worker.WorkerFactory

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val options = WorkflowServiceStubsOptions.newBuilder()
            .setTarget("temporal-frontend:7233")
            .build()
        val service = WorkflowServiceStubs.newInstance(options)
        val client = WorkflowClient.newInstance(service)
        val factory = WorkerFactory.newInstance(client)
        val worker: Worker = factory.newWorker(Queue.HELLO_TASK_QUEUE)
        worker.registerWorkflowImplementationTypes(GreetingWorkflowImpl::class.java)
        worker.registerActivitiesImplementations(HelloActivityImpl())
        factory.start()
    }
}
